DEBUG = true;

if (DEBUG)
	REDIRECT_TO_DEBUG = function (x) { document.getElementById("debug").innerHTML = x; }
else
	REDIRECT_TO_DEBUG = function (x) {}


// signature : getXhr()
// description : récupère un objet xhr afin de lancer une requête AJAX par la suite
// output : XMLHTTP object

function getXhr()
{
	var xhr = null ;
	if (window.XMLHttpRequest) // Firefox et autres
		xhr = new XMLHttpRequest() ;
	else if (window.ActiveXObject)
	{
		// Internet Explorer <7
		try
		{
			xhr = new ActiveXObject("Msxml2.XMLHTTP") ;
		}
		catch (e)
		{
			xhr = new ActiveXObject("Microsoft.XMLHTTP") ;
		}
	}
	else
	{
		// XMLHttpRequest non supporté par le navigateur
		alert("Votre navigateur ne supporte pas les objets XMLHTTP Request...") ;
		xhr = false ;
	}
	return xhr;
}


// signature : function post_request_text(file, to_send, action)
// description : envoie une requête ajax en post vers un fichier et défini ce que l'on fera de la réponse texte
// parameters :
// 		- file : chemin du fichier php à appeller 
// 		- to_send : string contenant l'ensemble des paramètres à envoyer en POST
// 		- action : fonction qui détermine ce qu'on fera du retour texte

	// exemple d'appel
// post_request_text(
// 	"page.php",
// 	"debug="+DEBUG.toString()+"&"+
// 	"param1="+"var1"+"&"+
// 	"param2="+"var2"+"&"+
// 	"param3="+"var3",
// 	function (data) { document.getElementById("debug").innerHTML = data; }
// 	) ;

function post_request_text(file, to_send, action)
{
	var xhr = getXhr() ;
	// On définit ce que l'on va faire quand on aura la réponse
	xhr.onreadystatechange = function()
	{
		// On ne fait quelque chose que si on a tout reçu et que le serveur est ok
		if(this.readyState == 4 && this.status == 200)
		{
			// alert(this.responseText);
			action(this.responseText) ;
		}
	}
	xhr.open("POST", file, true) ;
	xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8") ;
	xhr.send(to_send) ;
}


// signature : function get_request_text(file, to_send, action)
// description : envoie une requête ajax en get vers un fichier et défini ce que l'on fera de la réponse texte
// parameters :
// 		- file : chemin du fichier php à appeller 
// 		- to_send : string contenant l'ensemble des paramètres à envoyer en POST
// 		- action : fonction qui détermine ce qu'on fera du retour texte

	// exemple d'appel
// get_request_text(
// 	"page.php",
// 	"debug="+DEBUG.toString()+"&"+
// 	"param1="+"var1"+"&"+
// 	"param2="+"var2"+"&"+
// 	"param3="+"var3",
// 	function (data) { document.getElementById("debug").innerHTML = data; }
// 	) ;

function get_request_text(file, to_send, action)
{
	var xhr = getXhr() ;
	// On définit ce que l'on va faire quand on aura la réponse
	xhr.onreadystatechange = function()
	{
		// On ne fait quelque chose que si on a tout reçu et que le serveur est ok
		if(this.readyState == 4 && this.status == 200)
		{
			// alert(this.responseText);
			action(this.responseText) ;
		}
	}
	xhr.open("GET", file+"?"+to_send, true) ;
	xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8") ;
	xhr.send();
}


// $("button").click(function(){
//     $.get("demo_test.asp", function(data, status){
//         alert("Data: " + data + "\nStatus: " + status);
//     });
// }); 


// ------------------------------ FONTIONS DE REDIRECTION ----------------------------------------------------------------
					// Elles appellent les fonctions php du même nom


// signature :
// description :
// parameters :
// output :

function redirect(balise)
{
	return function (x) { document.getElementById(balise).innerHTML = x; };
}

function redirect_lib_php(fichier, appel, balise)
{
	post_request_text
	(
		fichier,
		"debug="+DEBUG.toString()+"&"+
		"appel="+appel,
		redirect(balise)
	);
}


function redirect_lib_php_param(fichier, appel, balise, param)
{
	post_request_text
	(
		fichier,
		"debug="+DEBUG.toString()+"&"+
		"appel="+appel+"&"+
		"param="+param,
		redirect(balise)
	);
}


function redirect_lib_php_multi_param(fichier, appel, balise, params)
{
	params_str = "";
	for (var i = 0; i < params.length-1; i++) {
		params_str += "param"+i.toString()+"="+params[i]+"&";
	}
	params_str += "param"+(params.length-1).toString()+"="+params[params.length-1];
	post_request_text
	(
		fichier,
		"debug="+DEBUG.toString()+"&"+
		"appel="+appel+"&"+
		params_str,
		redirect(balise)
	);
}