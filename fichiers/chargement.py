#!/usr/bin/env python
# -*- coding: utf-8 -*-

import mysql.connector 

conn = mysql.connector.connect(host="localhost",user="root",password="eisti0001", database="stage")
cursor = conn.cursor()

cursor.execute("""
CREATE TABLE IF NOT EXISTS Pays (
    ID int(5) NOT NULL AUTO_INCREMENT,
    nomPays varchar(50) UNIQUE,
    PRIMARY KEY(ID)
);
""")

cursor.execute("""
CREATE TABLE IF NOT EXISTS Entreprise (
    ID int(5) NOT NULL AUTO_INCREMENT,
    nomEntreprise varchar(255) UNIQUE,
    PRIMARY KEY(ID)
);
""")

cursor.execute("""
CREATE TABLE IF NOT EXISTS Ville (
    ID int(5) NOT NULL AUTO_INCREMENT,
    nomVille varchar(50),
    CodePostal varchar(10),
    idPays int(5),
    FOREIGN KEY fk_Pays(idPays) REFERENCES Pays(ID),
    PRIMARY KEY(ID)
);
""")

cursor.execute("""
CREATE TABLE IF NOT EXISTS Etudiant (
    ID int NOT NULL,
    idVille int(5),
    FOREIGN KEY fk_Ville(idVille) REFERENCES Ville(ID),
    PRIMARY KEY(ID)
);
""")

cursor.execute("""
CREATE TABLE IF NOT EXISTS Promotion (
    ID int(5) NOT NULL AUTO_INCREMENT,
    libelle varchar(25),
    PRIMARY KEY(ID)
);
""")

cursor.execute("""
CREATE TABLE IF NOT EXISTS Specialite (
    ID int(5) NOT NULL AUTO_INCREMENT,
    libelle varchar(25),
    idPromotion int(5),
    FOREIGN KEY fk_Promotion(idPromotion) REFERENCES Promotion(ID),
    PRIMARY KEY(ID)
);
""")

cursor.execute("""
CREATE TABLE IF NOT EXISTS FaireStage (
    idEntreprise int(5),
    idEtudiant int(10),
    idPromotion varchar(25),
    anneeStage varchar(100),
    remuneration decimal(6,2),
    CONSTRAINT pk_Embaucher PRIMARY KEY (idEntreprise,idEtudiant,idPromotion),
    FOREIGN KEY fk_Entreprise(idEntreprise) REFERENCES Entreprise(ID),
    FOREIGN KEY fk_Etudiant(idEtudiant) REFERENCES Etudiant(ID)
);
""")

cursor.execute("""
CREATE TABLE IF NOT EXISTS Appartenir (
    idSpecialite int(5),
    idEtudiant int(10),
    anneePromo varchar(100),
    campus varchar(50),
    CONSTRAINT pk_Appartenir PRIMARY KEY (idSpecialite,idEtudiant,anneePromo),
    FOREIGN KEY fk_Specialite(idSpecialite) REFERENCES Specialite(ID),
    FOREIGN KEY fk_Etudiant(idEtudiant) REFERENCES Etudiant(id)
);
""")

cursor.execute("""
CREATE TABLE IF NOT EXISTS Localiser (
    idEntreprise int(5),
    idVille int(5),
    CONSTRAINT pk_Localiser PRIMARY KEY (idEntreprise,idVille),
    FOREIGN KEY fk_Entreprise(idEntreprise) REFERENCES Entreprise(ID),
    FOREIGN KEY fk_Pays(idVille) REFERENCES Ville(id)
);
""")

cursor.execute("""
CREATE TABLE IF NOT EXISTS Users (
    username varchar(25),
    password varchar(25),
    permission int(1),
    PRIMARY KEY(username)
);
""")

#Remplissage user
cursor.execute("""SELECT username FROM Users WHERE username='test'""")
rows = cursor.fetchall()
if rows==[]:
    cursor.execute("""INSERT INTO Users (username,password,permission) VALUES('test','testpass','0')""")
cursor.execute("""SELECT username FROM Users WHERE username='admin'""")
rows = cursor.fetchall()
if rows==[]:
    cursor.execute("""INSERT INTO Users (username,password,permission) VALUES('admin','adminpass','1')""")

f_adresse=open("ADR_ELEVES_2017_2018_DATA_TABLE.txt","r")
f_niveau=open("PRG_ELV_SIT_2017_2018_DATA_TABLE_ANO.txt","r")
f_stage=open("STAGES_ELEVES_2013_2018_DATA_TABLE.txt","r")
#f_adresse.readline()
#f_niveau.readline()
#f_stage.readline()
l_adresse=[]
l_stage=[]
l_niveau=[]

entete_adresse=[]
entete_niveau=[]
entete_stage=[]




#Remplissage dico fichier des adresses
tmp=f_adresse.readlines()
for k in range(0,len(tmp)):
    tmp[k].split
    l_adresse.append({})
    if (k==0):
        for valeur in tmp[k].split("\t"):
            entete_adresse.append(valeur.replace("\r\n",""))
    else:
        i=0
        for valeur in tmp[k].split("\t"):
            l_adresse[k][entete_adresse[i]]=valeur.replace("\r\n","")
            i+=1
        #Remplir Pays
        cursor.execute("""SELECT nomPays FROM Pays WHERE nomPays=%(ADR_PAYS)s""", l_adresse[k])
        rows = cursor.fetchall()
        if rows==[]:
            cursor.execute("""INSERT INTO Pays (nomPays) VALUES(%(ADR_PAYS)s)""", l_adresse[k])
        #remplir Ville
        if l_adresse[k]["ADR_VILLE"].find("Cergy")!=-1:
            l_adresse[k]["ADR_VILLE"]="Cergy"
        if l_adresse[k]["ADR_VILLE"].find("Pau")!=-1:
            l_adresse[k]["ADR_VILLE"]="Pau"
        cursor.execute("""SELECT nomVille FROM Ville WHERE nomVille=%(ADR_VILLE)s""", l_adresse[k])
        rows = cursor.fetchall()
        if rows==[]:
            cursor.execute("""INSERT INTO Ville (nomVille,CodePostal,idPays) VALUES(%(ADR_VILLE)s,%(ADR_CP)s,(SELECT ID FROM Pays WHERE nomPays=%(ADR_PAYS)s))""", l_adresse[k])
        #Remplir Etudiant
        cursor.execute("""SELECT ID FROM Etudiant WHERE ID=%(ID_ANO)s""", l_adresse[k])
        rows = cursor.fetchall()
        if rows==[]:
            cursor.execute("""INSERT INTO Etudiant (ID,idVille) VALUES(%(ID_ANO)s,(SELECT ID FROM Ville WHERE nomVille=%(ADR_VILLE)s AND CodePostal=%(ADR_CP)s AND idPays=(SELECT ID FROM Pays WHERE nomPays=%(ADR_PAYS)s)))""", l_adresse[k])


    
#Remplissage dico fichier de stage
tmp=f_stage.readlines()
for k in range(0,len(tmp)):
    tmp[k].split
    l_stage.append({})
    if (k==0):
        for valeur in tmp[k].split("\t"):
            entete_stage.append(valeur.replace("\r\n",""))
    else:
        i=0
        for valeur in tmp[k].split("\t"):
            l_stage[k][entete_stage[i]]=valeur.replace("\r\n","")
            i+=1
        #remplir Entreprise
        cursor.execute("""SELECT nomEntreprise FROM Entreprise WHERE nomEntreprise=%(ENTREPRISE)s""", l_stage[k])
        rows = cursor.fetchall()
        if rows==[]:
            cursor.execute("""INSERT INTO Entreprise (nomEntreprise) VALUES(%(ENTREPRISE)s)""", l_stage[k])
        #Remplir Pays
        l_stage[k]["PAYS"]=l_stage[k]["PAYS"].lower()
        if l_stage[k]["PAYS"]=="be":
            l_stage[k]["PAYS"]="belgique"
        if l_stage[k]["PAYS"]=="brésil":
            l_stage[k]["PAYS"]="bresil"
        if l_stage[k]["PAYS"]=="brazil":
            l_stage[k]["PAYS"]="bresil"
        if l_stage[k]["PAYS"]=="england":
            l_stage[k]["PAYS"]="angleterre"
        if l_stage[k]["PAYS"]=="uk":
            l_stage[k]["PAYS"]="angleterre"
        if l_stage[k]["PAYS"]=="united kingdom":
            l_stage[k]["PAYS"]="angleterre"
        if l_stage[k]["PAYS"]=="etats-unis":
            l_stage[k]["PAYS"]="etats unis"
        if l_stage[k]["PAYS"]=="us":
            l_stage[k]["PAYS"]="etats unis"
        if l_stage[k]["PAYS"]=="usa":
            l_stage[k]["PAYS"]="etats unis"
        if l_stage[k]["PAYS"]=="germany":
            l_stage[k]["PAYS"]="allemagne"
        if l_stage[k]["PAYS"]=="india":
            l_stage[k]["PAYS"]="inde"
        if l_stage[k]["PAYS"]=="ireland":
            l_stage[k]["PAYS"]="irelande"
        if l_stage[k]["PAYS"]=="italy":
            l_stage[k]["PAYS"]="italie"
        if l_stage[k]["PAYS"]=="japan":
            l_stage[k]["PAYS"]="japon"
        if l_stage[k]["PAYS"].find("china")!=-1:
            l_stage[k]["PAYS"]="chine"
        if l_stage[k]["PAYS"]=="russia":
            l_stage[k]["PAYS"]="russie"
        if l_stage[k]["PAYS"]=="scotland":
            l_stage[k]["PAYS"]="ecosse"
        if l_stage[k]["PAYS"]=="singapore":
            l_stage[k]["PAYS"]="singapour"
        if l_stage[k]["PAYS"]=="spain":
            l_stage[k]["PAYS"]="espagne"
        if l_stage[k]["PAYS"]=="viet nam":
            l_stage[k]["PAYS"]="vietnam"
        if l_stage[k]["PAYS"]=="london":
            l_stage[k]["PAYS"]="londre"
        cursor.execute("""SELECT nomPays FROM Pays WHERE nomPays=%(PAYS)s""", l_stage[k])
        rows = cursor.fetchall()
        if rows==[]:
            cursor.execute("""INSERT INTO Pays (nomPays) VALUES(%(PAYS)s)""", l_stage[k])
        #remplir Ville
        if l_stage[k]["VILLE"].find("Cergy"):
            l_stage[k]["VILLE"]="Cergy"
        if l_stage[k]["VILLE"].find("Pau"):
            l_stage[k]["VILLE"]="Pau"
        cursor.execute("""SELECT nomVille FROM Ville WHERE nomVille=%(VILLE)s""", l_stage[k])
        rows = cursor.fetchall()
        if rows==[]:
            cursor.execute("""INSERT INTO Ville (nomVille,CodePostal,idPays) VALUES(%(VILLE)s,%(CODE_POSTAL)s,(SELECT ID FROM Pays WHERE nomPays=%(PAYS)s))""", l_stage[k])
        #remplir Promotion
        l_stage[k]["ANNEE"]=l_stage[k]["ANNEE"].split(" ")[0]
        if l_stage[k]["ANNEE"]!="ING1" and l_stage[k]["ANNEE"]!="ING2" and l_stage[k]["ANNEE"]!="ING3" and l_stage[k]["ANNEE"]!="CPI1" and l_stage[k]["ANNEE"]!="CPI2":
            l_stage[k]["ANNEE"]="autres"
        cursor.execute("""SELECT libelle FROM Promotion WHERE libelle=%(ANNEE)s""", l_stage[k])
        rows = cursor.fetchall()
        if rows==[]:
            cursor.execute("""INSERT INTO Promotion (libelle) VALUES(%(ANNEE)s)""", l_stage[k])
        #remplir FaireStage
        cursor.execute("""SELECT idEntreprise,idEtudiant,idPromotion FROM FaireStage WHERE idEntreprise=(SELECT ID FROM Entreprise WHERE nomEntreprise=%(ENTREPRISE)s) AND idEtudiant=%(ID_ANO)s AND idPromotion=%(ANNEE)s""", l_stage[k])
        rows = cursor.fetchall()
        if rows==[]:
            l_stage[k]["REMUNERATION"]=l_stage[k]["REMUNERATION"].replace(",",".")
            if l_stage[k]["REMUNERATION"]=="":
                l_stage[k]["REMUNERATION"]="0"
            #si l'étudiant n'exite pas
            cursor.execute("""SELECT ID FROM Etudiant WHERE ID=%(ID_ANO)s""",l_stage[k])
            rows=cursor.fetchall()
            if rows==[]:
                cursor.execute("""INSERT INTO Etudiant (ID) VALUES(%(ID_ANO)s)""", l_stage[k])
            cursor.execute("""INSERT INTO FaireStage (idEntreprise,idEtudiant,idPromotion,anneeStage,remuneration) VALUES ((SELECT ID FROM Entreprise WHERE nomEntreprise=%(ENTREPRISE)s),%(ID_ANO)s,%(ANNEE)s,%(ANNEE_SCOLAIRE)s,%(REMUNERATION)s)""", l_stage[k])
        #remplir localiser
        cursor.execute("""SELECT idEntreprise,idVille FROM Localiser WHERE idEntreprise=(SELECT ID FROM Entreprise WHERE nomEntreprise=%(ENTREPRISE)s) AND idVille=(SELECT ID FROM Ville WHERE nomVille=%(VILLE)s)""", l_stage[k])
        rows = cursor.fetchall()
        if rows==[]:
            cursor.execute("""INSERT INTO Localiser (idEntreprise,idVille) VALUES((SELECT ID FROM Entreprise WHERE nomEntreprise=%(ENTREPRISE)s),(SELECT ID FROM Ville WHERE nomVille=%(VILLE)s))""", l_stage[k])
        
#Remplissage dico fichier promotion
tmp=f_niveau.readlines()
for k in range(0,len(tmp)):
    tmp[k].split
    l_niveau.append({})
    if (k==0):
        for valeur in tmp[k].split("\t"):
            entete_niveau.append(valeur.replace("\n",""))
    else:
        i=0
        for valeur in tmp[k].split("\t"):
            l_niveau[k][entete_niveau[i]]=valeur.replace("\n","")
            i+=1
        #remplir Specialite
        tmp2=""
        if len(l_niveau[k]["PRG"].split("-"))>1:
            tmp2=l_niveau[k]["PRG"].split("-")[1]
        tmp3=""
        if l_niveau[k]["PRG"].find("CPI1")!=-1 or l_niveau[k]["PRG"].find("CPI2")!=-1:
            tmp3=l_niveau[k]["PRG"].split("-")[0]
        elif l_niveau[k]["PRG"]=="IFI" or l_niveau[k]["PRG"]=="DS" or l_niveau[k]["PRG"]=="SIE" or l_niveau[k]["PRG"]=="VISUAL" or l_niveau[k]["PRG"]=="SMART" or l_niveau[k]["PRG"]=="SIE" or l_niveau[k]["PRG"]=="ICC" or l_niveau[k]["PRG"]=="BI" or l_niveau[k]["PRG"]=="IERP" or l_niveau[k]["PRG"]=="IMSI" or l_niveau[k]["PRG"]=="FINTECH":
            tmp3="ING3"
        elif tmp2=="GI" or tmp2=="GM":
            l_niveau[k]["PRG"]=tmp2
            tmp3="ING1"
        elif l_niveau[k]["PRG"]=="MAIN" or l_niveau[k]["PRG"]=="MAAP":
            tmp3="ING2"
        elif  l_niveau[k]["PRG"]!="ING1" and l_niveau[k]["PRG"]!="ING2" and l_niveau[k]["PRG"]!="ING1" and l_niveau[k]["PRG"]!="GI":
            tmp3='autres'
        if tmp3!="":
            cursor.execute("""SELECT libelle FROM Specialite WHERE libelle=%(PRG)s""", l_niveau[k])
            rows = cursor.fetchall()
            if rows==[]:
                if tmp3=="ING1":
                    cursor.execute("""INSERT INTO Specialite (libelle,idPromotion) VALUES(%(PRG)s,(SELECT ID FROM Promotion WHERE libelle='ING1'))""", l_niveau[k])
                elif tmp3=="ING2":
                    cursor.execute("""INSERT INTO Specialite (libelle,idPromotion) VALUES(%(PRG)s,(SELECT ID FROM Promotion WHERE libelle='ING2'))""", l_niveau[k])
                elif tmp3=="ING3":
                    cursor.execute("""INSERT INTO Specialite (libelle,idPromotion) VALUES(%(PRG)s,(SELECT ID FROM Promotion WHERE libelle='ING3'))""", l_niveau[k])
                elif tmp3=="CPI1":
                    cursor.execute("""INSERT INTO Specialite (libelle,idPromotion) VALUES(%(PRG)s,(SELECT ID FROM Promotion WHERE libelle='CPI1'))""", l_niveau[k])
                elif tmp3=="CPI2":
                    cursor.execute("""INSERT INTO Specialite (libelle,idPromotion) VALUES(%(PRG)s,(SELECT ID FROM Promotion WHERE libelle='CPI2'))""", l_niveau[k])
                elif tmp3=="autres":
                    cursor.execute("""INSERT INTO Specialite (libelle,idPromotion) VALUES(%(PRG)s,(SELECT ID FROM Promotion WHERE libelle='autres'))""", l_niveau[k])
        #Remplir Appartenir
        cursor.execute("""SELECT idEtudiant FROM Appartenir WHERE idEtudiant=%(ID_ANO)s AND idSpecialite=%(PRG)s AND anneePromo=%(ANNEE_SCOLAIRE)s""", l_niveau[k])
        rows=cursor.fetchall()
        if rows==[]:
            #si l'étudiant n'exite pas
            cursor.execute("""SELECT ID FROM Etudiant WHERE ID=%(ID_ANO)s""",l_niveau[k])
            rows=cursor.fetchall()
            if rows==[]:
                cursor.execute("""INSERT INTO Etudiant (ID) VALUES(%(ID_ANO)s)""", l_niveau[k])
            #seulement si la spécialité existe
            cursor.execute("""SELECT ID FROM Specialite WHERE libelle=%(PRG)s""",l_niveau[k])
            rows=cursor.fetchall()
            if rows!=[]:
                cursor.execute("""INSERT INTO Appartenir (idSpecialite,idEtudiant,anneePromo,campus) VALUES((SELECT ID FROM Specialite WHERE libelle=%(PRG)s),%(ID_ANO)s,%(ANNEE_SCOLAIRE)s,%(SITE)s)""", l_niveau[k])
f_adresse.close()
f_niveau.close()
f_stage.close()
conn.commit()
cursor.close()
conn.close()