function initMap() {
    var location1 = {lat: 43.318879, lng: -0.3640607};
    var location2 = {lat: 43.318879, lng: -0.00142};

    var origin1 = {lat: 55.93, lng: -3.118};
    var origin2 = 'Greenwich, England';
    var destinationA = 'Stockholm, Sweden';
    var destinationB = {lat: 50.087, lng: 14.421};

    
    var geocoder = new google.maps.Geocoder();
    
    var map = new google.maps.Map(
	document.getElementById('map'),
	{
	    center: location1,
	    zoom: 10,
	    mapTypeId: google.maps.MapTypeId.ROADMAP
        }
    );
    var geocoder = new google.maps.Geocoder();

    document.getElementById('submit').addEventListener('click', function() {
        geocodeAddress(geocoder, map);
    });
    document.getElementById('distance').addEventListener('click', function() {
        distance();
    });
    
    var marker1 = new google.maps.Marker(
	{
	    position: location1,
	    map: map
	}
    );
    var marker2 = new google.maps.Marker(
	{
	    position: location2,
	    map: map
	}
    );
}

function geocodeAddress(geocoder, resultsMap) {
    var address = document.getElementById('address').value;
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
		map: resultsMap,
		position: results[0].geometry.location
            });
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function distance() {
    
    var origin1 = document.getElementById('origin1').value;
    var destination1 = document.getElementById('destination1').value;
    
    var origin2 = document.getElementById('origin2').value;
    var destination2 = document.getElementById('destination2').value;
    
    var service = new google.maps.DistanceMatrixService;
    var geocoder = new google.maps.Geocoder;

    var destinationIcon = 'https://chart.googleapis.com/chart?' + 'chst=d_map_pin_letter&chld=D|FF0000|000000';
    var originIcon = 'https://chart.googleapis.com/chart?' + 'chst=d_map_pin_letter&chld=O|FFFF00|000000';
    
    service.getDistanceMatrix({
        origins: [origin1, origin2],
        destinations: [destination1, destination2],
        travelMode: 'DRIVING',
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
    }, function(response, status) {
        if (status !== 'OK') {
            alert('Error was: ' + status);
	    
        } else {
	    
            var originList = response.originAddresses;
            var destinationList = response.destinationAddresses;
            var outputDiv = document.getElementById('result');
	    
            outputDiv.innerHTML = '';

	    var showGeocodedAddressOnMap = function(asDestination) {
		var icon = asDestination ? destinationIcon : originIcon;
		return function(results, status) {
                    //if (status === 'OK') {
			//map.fitBounds(bounds.extend(results[0].geometry.location));
			//markersArray.push(new google.maps.Marker({
			    //map: map,
			    //position: results[0].geometry.location,
			    //icon: icon
			//}));
                    //} else {
			//alert('Geocode was not successful due to: ' + status)
                    //}
		};
	    };
		
		for (var i = 0; i < originList.length; i++) {
		    var results = response.rows[i].elements;
		    geocoder.geocode({'address': originList[i]},
				     showGeocodedAddressOnMap(false));
		    for (var j = 0; j < results.length; j++) {
			geocoder.geocode({'address': destinationList[j]},
					 showGeocodedAddressOnMap(true));
			if (results[j].status !== "OK"){
			    outputDiv.innerHTML += originList[i] + ' jusqu\'à ' + destinationList[j] +
				': ' + results[j].status + '<br>';
			} else {
			    
			    outputDiv.innerHTML += originList[i] + ' jusqu\'à ' + destinationList[j] +
				': ' + results[j].distance.text + ' en ' +
				results[j].duration.text + '<br>';
			}
		    }
		}
	}
    });

    //"https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Washington,DC&destinations=New+York+City,NY&key=AIzaSyAIeVpttVsiZqFNAyECpjc9zQqdJpDQVWA"

}
