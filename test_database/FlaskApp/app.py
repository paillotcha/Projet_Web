from flask import Flask, render_template, json, request, session, redirect, url_for
import MySQLdb


app = Flask(__name__)
app.secret_key = 'secretkey'

conn = MySQLdb.connect(host="localhost",
                       user="root",
                       passwd="eisti0001",
                       db="db_stat")
cur = conn.cursor()

@app.route("/")
def main():
    
    session["username"] = None
    session["admin"] = False
    
    return render_template('accueil.html')

@app.route("/connexion")
def connexion():    
    return render_template("signup.html")

@app.route("/signUp",methods=['POST'])
def signUp():
    
    # read the posted values from the UI
    _name = request.form['username']
    _password = request.form['password']
    
    # validate the received values
    if _name and _password:

        result = cur.execute("SELECT * from users WHERE userName = %s AND userPass = %s", [_name, _password])

        print(result)

        if int(result) > 0:

            role = cur.execute("SELECT * from users WHERE userName = %s AND userPass = %s AND role = 1", [_name, _password])
            
            session["username"] = _name
            
            if int(role) > 0:
                session["admin"] = True
                return json.dumps({'message':'Loggin sucess !ADMIN'})
            else:
                session["admin"] = False
                return render_template("stats.html")
        else:
            return render_template("signup.html",error=1)
    else:
        return render_template("signup.html",error=2)
    

@app.route("/stats", methods=['POST'])
def stats():
    if (session["username"] == None):
        return render_template("signup.html",error=3)
    else:
        chartselect = request.form['chartselect']

        if chartselect:
            
            if (chartselect == "lieu"):
                return render_template("stats.html", chartdisp = 1)
            elif (chartselect == "remuneration"):
                return render_template("stats.html", chartdisp = 2)
            elif (chartselect == "promotion"):
                return render_template("stats.html", chartdisp = 3)
            elif (chartselect == "annee"):
                return render_template("stats.html", chartdisp = 4)
        else:
            return render_template("stats.html")

@app.route("/disconect")
def disconect():
    session.clear()
    print("sucessfull disconected...")
    print("redirect to signup.html")
    
    session["username"] = None
    session["admin"] = False
    
    return redirect("/connexion")

if __name__ == "__main__":
    app.run()
